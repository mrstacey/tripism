var windowWidth = $(window).width();
$(window).on('load', function() {
	var timer;
	
	$(window).scroll(function() {
		
		var scrolled = $(window).scrollTop();
		
		var homeLoc = $('#home').offset().top - scrolled
		var worksLoc = $('#works').offset().top - scrolled
		var corporationsLoc = $('#corporations').offset().top - scrolled
		var suppliersLoc = $('#suppliers').offset().top - scrolled
		var contactLoc = $('#contact').offset().top - scrolled
		
		if(timer) {
			window.clearTimeout(timer);
		}
		timer = window.setTimeout(function() {
			
			if(scrolled >= 70 ){
				$('.navbar').css('background-color','rgba(0,0,0,0.5)');
			}else{
				$('.navbar').css('background-color','transparent');
			}
			
			if(homeLoc <= 70 ){
				$('#main-menu').find('li.active').removeClass('active');
				$('#main-menu').find('li.home').addClass('active');
				updateMagicLine();
			}
		
			if(worksLoc <= 70 ){
				if(!$('#main-menu').find('li.works').hasClass('active')){
					$('#main-menu').find('li.active').removeClass('active');
					$('#main-menu').find('li.works').addClass('active');
					updateMagicLine();
				}
			}
			if(corporationsLoc <= 70 ){
				if(!$('#main-menu').find('li.corporations').hasClass('active')){
					$('#main-menu').find('li.active').removeClass('active');
					$('#main-menu').find('li.corporations').addClass('active');
					updateMagicLine();
				}
			}
			if(suppliersLoc <= 70 ){
				if(!$('#main-menu').find('li.suppliers').hasClass('active')){
					$('#main-menu').find('li.active').removeClass('active');
					$('#main-menu').find('li.suppliers').addClass('active');
					updateMagicLine();
				}
			}
			if(contactLoc <= 70 ){
				$('#main-menu').find('li.active').removeClass('active');
				updateMagicLine();
			}
		}, 20)
	});
	
});

$( window ).resize(function() {
	makeLines();
	if(windowWidth >= 768){
		buildSlider();
		startLogoAnimiation()
	}
	
});

function buildSlider(){
	var visibleSlides = 7;
	var newWidth = windowWidth/visibleSlides;
	var slideCount = $('.logo-slider .slide').length;
	var sliderPos = $('.logo-wrapper').position().left;
	var middleSlide = Math.ceil(slideCount/2);
	$('.logo-slider .slide').width(newWidth).css('flex-basis', newWidth+'px');
	$('.logo-wrapper').width(middleSlide*newWidth);
	$('.logo-slider .slide').eq(middleSlide-1).after('<div class="w-100"></div>')
}

function startLogoAnimiation(){
	var animationDuration = 10000;
	var animationLength = $('.logo-slider .slide').width()*-1
	
	$('.logo-wrapper').animate({
		left: animationLength
	}, {
		easing:"linear",
        duration: animationDuration,
        step: function (currentLeft,fx) {},
        complete: function(){
	        
			var slideWidth = $('.slide:first-child').width();
			$('.logo-wrapper').width($('.logo-wrapper').width()+slideWidth);
	        $('.slide:first-child').clone().insertBefore($('.logo-slider .w-100'));
	        $('.slide:first-child').remove();
	        
	        var lineBreak = $('.logo-slider .w-100').index();
			$('.slide').eq(lineBreak).clone().appendTo('#logo-list');
			$('.slide').eq(lineBreak).remove();
			
			$('.logo-wrapper').css('left','0px');
			
	        startLogoAnimiation();
	    }
    }) 
}


$(document).ready(function(){
	if(windowWidth >= 768){
		buildSlider();
		startLogoAnimiation();
	}

	
	$('.logo-slider').find('img').each(function(){
		var orgWidth = $(this).width();
		if(windowWidth < 960){
			$(this).css('width', orgWidth/3 +'px');
		}else{
			$(this).css('width', orgWidth/2 +'px');
		}
	});
	
	$('#main-menu').append('<div id="magicline"></div>');
	var $magicLine = $("#magicline");
	updateMagicLine();
	
	$(".navbar li").hover(function() {
        $el = $(this);
        leftPos = $el.position().left;
        newWidth = $el.width();
        $magicLine.stop().animate({
            left: leftPos,
            width: newWidth
        });
    }, function() {
        $magicLine.stop().animate({
            left: $magicLine.data("origLeft"),
            width: $magicLine.data("origWidth")
        });    
    });
    $('#navbarNav a').click(function(){
		if($( $.attr(this, 'href') ).length){
			$('html, body').animate({
				scrollTop: $( $.attr(this, 'href') ).offset().top
			}, 500);
			$('#main-menu').find('.active').removeClass('active');
			$(this).parent().addClass('active');
			$('.navbar-collapse').addClass('collapsed').removeClass('show');
			$('.navbar-toggler').addClass('collapsed');
			$('body').removeClass('overlay-open');
			$('.overlay').remove();
			return false;
		}
		
	});

	makeLines();
	
	$('.cloud-holder .dot-holder').append('<div class="comet"></div>');
	
	$(".navbar-toggler").addClass('collapsed');
	$('#navbarNav').on('show.bs.collapse', function(){
		$('body').append('<div class="overlay"></div>');
		$('body').addClass('overlay-open');
	})
	$('#navbarNav').on('hide.bs.collapse', function(){
		$('.overlay').remove();
		$('body').removeClass('overlay-open');
	})
	$(".navbar-toggler").on("click", function () {
		$(this).toggleClass("active");
		
	});
	$(document).on('click',function(){
		if(!$(event.target).closest('.navbar-toggler').length) {
			if ($('.navbar-toggler').hasClass('active') ) {
				$('.collapse').collapse('hide');
				$(".navbar-toggler").toggleClass("active");
			}
		}
	});
	
	var forms = $('.needs-validation');
	var validation = Array.prototype.filter.call(forms, function(form) {
	  form.addEventListener('submit', function(event) {
	    if (form.checkValidity() === false) {
	      event.preventDefault();
	      event.stopPropagation();
	    }
	    form.classList.add('was-validated');
	  }, false);
	});
	
});
function updateMagicLine(){
    var $magicLine = $("#magicline");
	if($(".navbar li.active").length){
		$magicLine
			.show()
			.width($(".navbar li.active").width())
			.css("left", $(".navbar li.active").position().left)
			.data("origLeft", $magicLine.position().left)
			.data("origWidth", $magicLine.width());
	}else{
		$magicLine.hide();
	}
}

function makeLines(){
	
	var dotCoords = {};
	$('.cloud-holder .dot-holder .dot').each(function(){
 		dotCoords[$(this).index()+1] = {'x': $(this).position().left + ($(this).width()/2), 'y': $(this).position().top + ($(this).height()/2)};
	});
	
	$('#line1').attr('x1',dotCoords[9]['x']).attr('y1',dotCoords[9]['y']).attr('x2',dotCoords[17]['x']).attr('y2',dotCoords[17]['y']);
	$('#line2').attr('x1',dotCoords[9]['x']).attr('y1',dotCoords[9]['y']).attr('x2',dotCoords[12]['x']).attr('y2',dotCoords[12]['y']);
	$('#line3').attr('x1',dotCoords[9]['x']).attr('y1',dotCoords[9]['y']).attr('x2',dotCoords[18]['x']).attr('y2',dotCoords[18]['y']);
	$('#line4').attr('x1',dotCoords[9]['x']).attr('y1',dotCoords[9]['y']).attr('x2',dotCoords[19]['x']).attr('y2',dotCoords[19]['y']);
	$('#line5').attr('x1',dotCoords[17]['x']).attr('y1',dotCoords[17]['y']).attr('x2',dotCoords[10]['x']).attr('y2',dotCoords[10]['y']);
	$('#line6').attr('x1',dotCoords[17]['x']).attr('y1',dotCoords[17]['y']).attr('x2',dotCoords[7]['x']).attr('y2',dotCoords[7]['y']);
	$('#line7').attr('x1',dotCoords[17]['x']).attr('y1',dotCoords[17]['y']).attr('x2',dotCoords[1]['x']).attr('y2',dotCoords[1]['y']);
	$('#line8').attr('x1',dotCoords[10]['x']).attr('y1',dotCoords[10]['y']).attr('x2',dotCoords[18]['x']).attr('y2',dotCoords[18]['y']);
	$('#line9').attr('x1',dotCoords[10]['x']).attr('y1',dotCoords[10]['y']).attr('x2',dotCoords[2]['x']).attr('y2',dotCoords[2]['y']);
	$('#line10').attr('x1',dotCoords[10]['x']).attr('y1',dotCoords[10]['y']).attr('x2',dotCoords[5]['x']).attr('y2',dotCoords[5]['y']);
	$('#line11').attr('x1',dotCoords[2]['x']).attr('y1',dotCoords[2]['y']).attr('x2',dotCoords[20]['x']).attr('y2',dotCoords[20]['y']);
	$('#line12').attr('x1',dotCoords[2]['x']).attr('y1',dotCoords[2]['y']).attr('x2',dotCoords[4]['x']).attr('y2',dotCoords[4]['y']);
	$('#line13').attr('x1',dotCoords[2]['x']).attr('y1',dotCoords[2]['y']).attr('x2',dotCoords[5]['x']).attr('y2',dotCoords[5]['y']);
	$('#line14').attr('x1',dotCoords[2]['x']).attr('y1',dotCoords[2]['y']).attr('x2',dotCoords[11]['x']).attr('y2',dotCoords[11]['y']);
	$('#line15').attr('x1',dotCoords[10]['x']).attr('y1',dotCoords[10]['y']).attr('x2',dotCoords[8]['x']).attr('y2',dotCoords[8]['y']);
	$('#line16').attr('x1',dotCoords[18]['x']).attr('y1',dotCoords[18]['y']).attr('x2',dotCoords[2]['x']).attr('y2',dotCoords[2]['y']);
	$('#line17').attr('x1',dotCoords[18]['x']).attr('y1',dotCoords[18]['y']).attr('x2',dotCoords[20]['x']).attr('y2',dotCoords[20]['y']);
	$('#line18').attr('x1',dotCoords[19]['x']).attr('y1',dotCoords[19]['y']).attr('x2',dotCoords[20]['x']).attr('y2',dotCoords[20]['y']);
	$('#line19').attr('x1',dotCoords[19]['x']).attr('y1',dotCoords[19]['y']).attr('x2',dotCoords[8]['x']).attr('y2',dotCoords[8]['y']);
	$('#line20').attr('x1',dotCoords[8]['x']).attr('y1',dotCoords[8]['y']).attr('x2',dotCoords[20]['x']).attr('y2',dotCoords[20]['y']);
	$('#line21').attr('x1',dotCoords[8]['x']).attr('y1',dotCoords[8]['y']).attr('x2',dotCoords[4]['x']).attr('y2',dotCoords[4]['y']);
	$('#line22').attr('x1',dotCoords[20]['x']).attr('y1',dotCoords[20]['y']).attr('x2',dotCoords[4]['x']).attr('y2',dotCoords[4]['y']);
	$('#line23').attr('x1',dotCoords[4]['x']).attr('y1',dotCoords[4]['y']).attr('x2',dotCoords[5]['x']).attr('y2',dotCoords[5]['y']);
	$('#line24').attr('x1',dotCoords[5]['x']).attr('y1',dotCoords[5]['y']).attr('x2',dotCoords[11]['x']).attr('y2',dotCoords[11]['y']);
	$('#line25').attr('x1',dotCoords[5]['x']).attr('y1',dotCoords[5]['y']).attr('x2',dotCoords[7]['x']).attr('y2',dotCoords[7]['y']);
	$('#line26').attr('x1',dotCoords[5]['x']).attr('y1',dotCoords[5]['y']).attr('x2',dotCoords[6]['x']).attr('y2',dotCoords[6]['y']);
	$('#line27').attr('x1',dotCoords[7]['x']).attr('y1',dotCoords[7]['y']).attr('x2',dotCoords[15]['x']).attr('y2',dotCoords[15]['y']);
	$('#line28').attr('x1',dotCoords[7]['x']).attr('y1',dotCoords[7]['y']).attr('x2',dotCoords[6]['x']).attr('y2',dotCoords[6]['y']);
	$('#line29').attr('x1',dotCoords[7]['x']).attr('y1',dotCoords[7]['y']).attr('x2',dotCoords[1]['x']).attr('y2',dotCoords[1]['y']);
	$('#line30').attr('x1',dotCoords[7]['x']).attr('y1',dotCoords[7]['y']).attr('x2',dotCoords[14]['x']).attr('y2',dotCoords[14]['y']);
	$('#line31').attr('x1',dotCoords[3]['x']).attr('y1',dotCoords[3]['y']).attr('x2',dotCoords[11]['x']).attr('y2',dotCoords[11]['y']);
	$('#line32').attr('x1',dotCoords[3]['x']).attr('y1',dotCoords[3]['y']).attr('x2',dotCoords[1]['x']).attr('y2',dotCoords[1]['y']);
	$('#line33').attr('x1',dotCoords[3]['x']).attr('y1',dotCoords[3]['y']).attr('x2',dotCoords[13]['x']).attr('y2',dotCoords[13]['y']);
	$('#line34').attr('x1',dotCoords[3]['x']).attr('y1',dotCoords[3]['y']).attr('x2',dotCoords[15]['x']).attr('y2',dotCoords[15]['y']);
	$('#line35').attr('x1',dotCoords[1]['x']).attr('y1',dotCoords[1]['y']).attr('x2',dotCoords[11]['x']).attr('y2',dotCoords[11]['y']);
	$('#line36').attr('x1',dotCoords[1]['x']).attr('y1',dotCoords[1]['y']).attr('x2',dotCoords[12]['x']).attr('y2',dotCoords[12]['y']);
	$('#line37').attr('x1',dotCoords[16]['x']).attr('y1',dotCoords[16]['y']).attr('x2',dotCoords[12]['x']).attr('y2',dotCoords[12]['y']);
	$('#line38').attr('x1',dotCoords[16]['x']).attr('y1',dotCoords[16]['y']).attr('x2',dotCoords[1]['x']).attr('y2',dotCoords[1]['y']);
	$('#line39').attr('x1',dotCoords[13]['x']).attr('y1',dotCoords[13]['y']).attr('x2',dotCoords[14]['x']).attr('y2',dotCoords[14]['y']);
}